package com.rave.colorrandomizer

import androidx.compose.ui.graphics.Color
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rave.colorrandomizer.ui.theme.White
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.util.Random

class MainViewModel(private val random: Random) : ViewModel() {
    private val _color = MutableStateFlow(White)
    val color: StateFlow<Color> get() = _color

    fun randomizer() {
        val roy = random.nextInt(256)
        val gee = random.nextInt(256)
        val biv = random.nextInt(256)
        _color.value = Color(roy, gee, biv)
    }

    fun reset() {
        _color.value = White
    }

    companion object {

        /**
         * Anonymous example of [ViewModelProvider.Factory]
         *
         * @param random
         * @return
         */
        fun getFactory(random: Random): ViewModelProvider.Factory =
            object : ViewModelProvider.Factory {
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return MainViewModel(random) as T
                }
            }
    }
}

/**
 * Class example of [ViewModelProvider.Factory]
 *
 * @property random
 * @constructor Create empty Main view model factory
 */
class MainViewModelFactory(private val random: Random) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainViewModel(random) as T
    }
}