package com.rave.colorrandomizer

import androidx.compose.ui.graphics.Color
import com.rave.colorrandomizer.ui.theme.White
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.util.Random

internal class MainViewModelTest {

    private val random = mockk<Random>()
    private val mainViewModel = MainViewModel(random)

    @BeforeEach
    fun beforeEach() {
        every { random.nextInt(any()) } answers { 0 }
    }

    @Test
    @DisplayName("Testing that random color is returned")
    fun testRandomizer() {
        // Given
        val expected = Color(0, 0, 0)

        // When
        mainViewModel.randomizer()

        // Then
        Assertions.assertEquals(expected, mainViewModel.color.value)
    }


    @Test
    @DisplayName("Testing that color is reset back to default")
    fun testReset() {
        // Given
        mainViewModel.randomizer()
        Assertions.assertNotEquals(White, mainViewModel.color.value)

        // When
        mainViewModel.reset()

        // Then
        Assertions.assertEquals(White, mainViewModel.color.value)
    }
}